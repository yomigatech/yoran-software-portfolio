# Software Development Portfolio
![Yoran Marchione](http://www.gravatar.com/avatar/62c99188874a5e62d4b7e1ccc740b232.jpg?s=256)

*Yoran Marchione*

### About Portfolio
This portfolio contains a selection of works in games, apps and websites relevant to game and app development.

Most of the game content is compatible with Windows OS only.

[Download Repository](https://bitbucket.org/yomigatech/yoransoftwareportfolio/downloads)

### About Me
I am a hobbyist game developer and designer looking to enter the games industry. I am specifically interested in developing and designing VR games.

### Contact Information
Please contact Yoran Marchione at <yoran@yomigatech.com>

### Experience
##### Game Development
* Unity 3D
* SDL
* Love2D
##### Programming Languages
* C#
* Java
* C/C++
* Lua
* PHP
##### Development Tools
* git
* Atlassian Bitbucket
* Atlassian JIRA
* Atlassian Confluence
* Vim
* Android Studio
* MonoDevelop
* Visual Studio Code
##### 3D Modeling | Rendering | Animation
* Blender 3D
* Autodesk Maya
* Autodesk 3DS Max
* Cinema 4D
##### Video Editing | Compositing
* Adobe Premiere
* Adobe After Effects
* Blender 3D
##### Audio Production
* FL Studio
* Native Instruments
* Audacity
* Sonar
##### Photo Editing
* Adobe Photoshop
* GIMP
##### Vector Art
* Adobe Illustrator
* Inkscape
##### Pixel Art | Animation
* Aseprite

---

[yomiga Tech LLC](http://yomigatech.com) | Copyright 2017
